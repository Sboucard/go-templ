# Template: GO + Templ + GO-i18n + HTMX + Tailwind

> The branch `project-template` can be retrieved to start a new web project in Go, using templ to create and render reusable html component, that can be dynamic thanks to HTMX.

## Stacks

### Front-end

- [HTMX](https://htmx.org/)
- [Tailwind](https://tailwindcss.com/)
- [Templ](https://templ.guide/)

### Back-end

- [GO](https://go.dev/)

## Tools

### Linting

- [commitlint](https://commitlint.js.org/)

### Localization

- [go-i18next](https://github.com/yuangwei/go-i18next)

### Package management

- [Npm](npmjs.com/)
- [GO](https://go.dev/)

### Dev server

- [Air](https://github.com/cosmtrek/air)

## Why ?

> "Why not using popular `javascript` framework such as React or Vue in order to create a simple web application ?"

Well the question is relevant, nowadays a lot of web projects are done using the same framework for the front-end and for the back-end. `Next.js` is good example, since you can work with the same language for the front and for the back. And this template is no different but instead of using `javascript` or `typescript`, you can use `golang`.

There is currently a problem with front-end framework, which is the duplication of data and source of truth. I do think that in the end, we need to thrive for light and fast server web app.

This project is to give golang a shot. To see what's possible to do with the language and tools provided by
the community, while testing new libraries such as for example `HTMX` and older ones such as `Tailwindcss`. With the goal in mind to create a lightweight and fast web app.

## Folder structure

```fish
.
├── assets
│  └── js
│     └── htmx.min.js
├── commitlint.config.js
├── components
│  ├── app
│  │  ├── app.templ
│  │  └── app_templ.go
│  ├── card
│  │  ├── card.templ
│  │  └── card_templ.go
│  ├── generate.go
│  ├── layout
│  │  ├── button.templ
│  │  ├── button_templ.go
│  │  ├── card.templ
│  │  ├── card_templ.go
│  │  ├── page.templ
│  │  ├── page_templ.go
│  │  ├── todo.templ
│  │  └── todo_templ.go
│  ├── menu
│  │  ├── menu.templ
│  │  └── menu_templ.go
│  ├── page
│  │  ├── page.templ
│  │  └── page_templ.go
│  └── todo
│     ├── todo.templ
│     └── todo_templ.go
├── Dockerfile
├── go.mod
├── go.sum
├── handlers
│  └── default.go
├── helper
│  ├── helperI18n
│  │  └── i18n.go
│  └── helperServices
│     └── services.go
├── husky
│  └── _
│     ├── applypatch-msg
│     ├── commit-msg
│     ├── h
│     ├── husky.sh
│     ├── post-applypatch
│     ├── post-checkout
│     ├── post-commit
│     ├── post-merge
│     ├── post-rewrite
│     ├── pre-applypatch
│     ├── pre-auto-gc
│     ├── pre-commit
│     ├── pre-push
│     ├── pre-rebase
│     └── prepare-commit-msg
├── i18next
│  └── i18next.go
├── locales
│  ├── en
│  │  └── locale.json
│  ├── fr
│  │  └── locale.json
│  └── jp
│     └── locale.json
├── main.go
├── middleware
│  └── cache.go
├── model
│  ├── model.go
│  └── svgPaths.go
├── package-lock.json
├── package.json
├── README.md
├── services
│  ├── aboutService
│  │  └── about.go
│  ├── buttonsService
│  │  └── buttons.go
│  ├── cardViewsService
│  │  └── cardViews.go
│  ├── careerService
│  │  └── career.go
│  ├── pagesService
│  │  └── pages.go
│  ├── projectService
│  │  └── projects.go
│  └── stacksService
│     └── stacks.go
├── static
│  ├── css
│  │  ├── jetbrains-mono.css
│  │  └── tailwind.css
│  ├── fonts
│  │  ├── JetBrainsMono-Bold.woff2
│  │  ├── JetBrainsMono-BoldItalic.woff2
│  │  ├── JetBrainsMono-ExtraBold.woff2
│  │  ├── JetBrainsMono-ExtraBoldItalic.woff2
│  │  ├── JetBrainsMono-ExtraLight.woff2
│  │  ├── JetBrainsMono-ExtraLightItalic.woff2
│  │  ├── JetBrainsMono-Italic.woff2
│  │  ├── JetBrainsMono-Light.woff2
│  │  ├── JetBrainsMono-LightItalic.woff2
│  │  ├── JetBrainsMono-Medium.woff2
│  │  ├── JetBrainsMono-MediumItalic.woff2
│  │  ├── JetBrainsMono-Regular.woff2
│  │  ├── JetBrainsMono-SemiBold.woff2
│  │  ├── JetBrainsMono-SemiBoldItalic.woff2
│  │  ├── JetBrainsMono-Thin.woff2
│  │  └── JetBrainsMono-ThinItalic.woff2
│  ├── icons
│  │  ├── box.svg
│  │  ├── chat-code.svg
│  │  ├── favicon.svg
│  │  ├── history.svg
│  │  ├── home.svg
│  │  ├── library.svg
│  │  └── link.svg
│  └── images
│     ├── flag_france.png
│     ├── flag_japan.png
│     ├── flag_uk.webp
│     ├── heading_angers.jpeg
│     ├── logo_angular.png
│     ├── logo_asp_core.png
│     ├── logo_azure_devops.png
│     ├── logo_csharp.png
│     ├── logo_docker.webp
│     ├── logo_git.png
│     ├── logo_github.png
│     ├── logo_gitlab.png
│     ├── logo_golang.png
│     ├── logo_jenkins.png
│     ├── logo_js.png
│     ├── logo_kvm.png
│     ├── logo_mongodb.png
│     ├── logo_neovim.png
│     ├── logo_nestjs.png
│     ├── logo_node.png
│     ├── logo_reactjs.webp
│     ├── logo_scrum.png
│     ├── logo_server.png
│     ├── logo_tailwind.svg
│     ├── logo_vs.png
│     ├── logo_vuejs.png
│     ├── logo_windows.webp
│     └── profile_picture.jpeg
├── tailwind.config.js
└── views
   ├── aboutView
   │  ├── AboutView.templ
   │  └── AboutView_templ.go
   ├── careerView
   │  ├── careerCardViews
   │  │  ├── careerCardBeconfig.templ
   │  │  ├── careerCardBeconfig_templ.go
   │  │  ├── careerCardChevrollier.templ
   │  │  ├── careerCardChevrollier_templ.go
   │  │  ├── careerCardDav.templ
   │  │  ├── careerCardDav_templ.go
   │  │  ├── careerCardIIA.templ
   │  │  ├── careerCardIIA_templ.go
   │  │  ├── careerCardTelelogos.templ
   │  │  └── careerCardTelelogos_templ.go
   │  ├── careerView.templ
   │  └── careerView_templ.go
   ├── generate.go
   ├── homeView
   │  ├── homeView.templ
   │  └── homeView_templ.go
   ├── projectsView
   │  ├── projectsView.templ
   │  └── projectsView_templ.go
   └── stacksView
      ├── stacksView.templ
      └── stacksView_templ.go
```

## What does this template

It contain an example of boilerplate component in `app.templ` to create your components.
There is a i18n helper for locales with `yuangwei/go-i18n` and an example of how you can integrate it inside your `templ` components. `Air` generate the go file from the `*.templ` file when executed.

It provide `commitlint` if you need to create and/or specify standard for your commits.

## Launch App

To launch your application do the following :

### Download the GO dependencies

```fish:
go mod tidy
```

### Download npm packages

```fish
npm install
```

### Serve the app

```fish
air server --port 8080
```

## What's next ?

You're free to use the tool that suit your need in order to deploy your application.

If your thinking of creating a PWA, I do think that [Web Components](https://developer.mozilla.org/en-US/docs/Web/API/Web_components) is the way to go. Or use a cache strategy.
