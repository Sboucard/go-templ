/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./components/**/*.templ", "./views/**/*.templ"],
  theme: {
    screens: {
      sm: "480px",
      md: "768px",
      lg: "976px",
      xl: "1440px",
    },
    colors: {
      "tokyo-header-1": "#f7768e",
      "tokyo-header-2": "#ff9e64",
      "tokyo-header-3": "#9ece6a",
      "tokyo-blue": "#7dcfff",
      "tokyo-violet": "#bb9af7",
      "tokyo-standard": "#c0caf5",
      "tokyo-small": "#a9b1d6",
      "tokyo-hr": "#9aa5ce",
      "tokyo-hr-bis": "#565f89",
      "tokyo-cream": "#cfc9c2",
      "tokyo-container": "#414868",
      "tokyo-content": "#24283b",
      "tokyo-menu": "#1a1b26",
    },
    extend: {
      fontFamily: {
        mono: ["JetBrains Mono", "monospace"],
      },
      keyframes: {
        fadein: {
          "0%": { opacity: "0", transform: "translateY(80%)" },
          "20%": { opacity: "0" },
          "50%": { opacity: "1", transform: "translateY(0%)" },
          "100%": { opacity: "1", transform: "translateY(0%)" },
        },
      },
      animation: {
        fadein: "2s fadein ease-out linear",
      },
    },
  },
  plugins: [
    function ({ addUtilities }) {
      const customClasses = {
        ".rounded-img": {
          clipPath: "circle(48%)",
        },
        ".list-arrow": {
          "list-style-type": "none",
          "& li::before": {
            content: '"> "',
            "padding-right": "5px",
            "font-weight": "bold",
          },
        },
        ".scrollbar-thin-card": {
          scrollbarWidth: "thin",
          scrollbarColor: "#24283b transparent",
        },
        ".scrollbar-thin-page": {
          scrollbarWidth: "thin",
          scrollbarColor: "#bb9af7 transparent",
        },
        ".scrollbar-webkit-card": {
          "&::-webkit-scrollbar": {
            scrollbarWidth: "8px",
          },
          "&::-webkit-scrollbar-track": {
            background: "transparent",
          },
          "&::-webkit-scrollbar-tumb": {
            backgroundColor: "#24283b",
            borderRadius: "16px",
            border: "1px solid #cfc9c2",
          },
        },
        ".scrollbar-webkit-page": {
          "&::-webkit-scrollbar": {
            scrollbarWidth: "8px",
          },
          "&::-webkit-scrollbar-track": {
            background: "transparent",
          },
          "&::-webkit-scrollbar-tumb": {
            backgroundColor: "#bb9af7",
            borderRadius: "16px",
            border: "1px solid #bb9af7",
          },
        },
      };
      addUtilities(customClasses, ["responsive", "hover"]);
    },
  ],
};
