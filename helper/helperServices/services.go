package helperServices

import (
	"cv/pwa/services/aboutService"
	"cv/pwa/services/appService"
	"cv/pwa/services/buttonsService"
	"cv/pwa/services/cardViewsService"
	"cv/pwa/services/careerService"
	"cv/pwa/services/pagesService"
	"cv/pwa/services/projectService"
	"cv/pwa/services/stacksService"
)

// Initialize and refresh components & views data
func ComponentStartupService() {
	appService.InitAppLink()
	buttonsService.InitButtons()
	pagesService.InitPages()
	pagesService.InitMap()
	careerService.InitCareerCards()
	careerService.InitMap()
	aboutService.InitTodo()
	stacksService.InitStackCards()
	projectService.InitProjectCards()
	cardViewsService.InitCardViews()
}
