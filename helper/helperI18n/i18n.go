package helperI18n

import (
	"cv/pwa/i18next"
	"fmt"
	"net/http"
	"strings"
)

// Global variable to register i18next bundle
var i18nextBundle i18next.I18n

// Setup default language and save the bundle
func RegisterI18NextBundle() {
	var errorI18n error

	i18nextBundle, errorI18n = i18next.Init(i18next.I18nOptions{
		Lng:        []string{"en", "fr"},
		DefaultLng: "en",
		Ns:         "json",
		Backend: i18next.Backend{
			LoadPath: []string{"./locales/{{.Lng}}/locale.json"},
		},
	})

	if errorI18n != nil {
		fmt.Println("error locale file : %w", errorI18n)
	}
}

// Change the locale if web app is accessed with a different language browser
func ChangeLangNext(w http.ResponseWriter, r *http.Request) {
	accept := r.Header.Get("Accept-Language")

	if strings.HasPrefix(accept, "fr") {
		i18nextBundle.ChangeLanguage("fr")
	} else if strings.HasPrefix(accept, "en") {
		i18nextBundle.ChangeLanguage("en")
	}
}

// Get the localized string from key
func UseLocaleString(key string) string {
	value, _ := i18nextBundle.T(key, struct {
		opts string
	}{})

	return value
}
