package model

var (
	SvgIconHomePath     string = "static/icons/home.svg"
	SvgIconCareerPath   string = "static/icons/history.svg"
	SvgIconAboutPath    string = "static/icons/chat-code.svg"
	SvgIconStacksPath   string = "static/icons/library.svg"
	SvgIconProjectsPath string = "static/icons/box.svg"
)
