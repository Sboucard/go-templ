package model

import "github.com/a-h/templ"

type AppLink struct {
	Title string
	URL   string
}

type Button struct {
	Title string
	Icon  string
	Route string
}

type CareerCard struct {
	Title        string
	Period       string
	Location     string
	Group        string
	BaseCardView templ.Component
	Content      CareerCardContent
}

type StackCard struct {
	ImageURL    string
	Title       string
	Mastery     string
	Description string
	Type        string
}

type CareerCardContent struct {
	ID     string
	Title  string
	Subcat []CareerSubcat
}

type CareerSubcat struct {
	Index          int
	Title          string
	Route          string
	CareerCardView templ.Component
}

type CompanyInfo struct {
	Duration string
	Projects []Project
	Stacks   []string
	Title    string
}

type Page struct {
	Title    string
	Views    []View
	BaseView templ.Component
}

type Project struct {
	Title string
}

type ProjectCard struct {
	Title    string
	Duration string
	Company  string
	AppURL   string
	Type     string
	Dates    string
	Role     string
}

type SchoolInfo struct {
	Duration string
	Fields   []string
}

type Todo struct {
	Label    string
	Checked  bool
	Disabled bool
}

type View struct {
	Label     string
	Component templ.Component
	Route     string
}
