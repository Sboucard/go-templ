# Builder
FROM golang:alpine3.19 AS builder
WORKDIR /go/src/app
COPY . ./

# Install npm and tailwind then clean itself after the build
RUN apk add --no-cache npm && \
  npx tailwindcss init -p && \
  npx tailwindcss build -o static/css/tailwind.css && \
  apk del npm && \
  rm -rf ~/var/cache/apk/*

# Build the web server then install dependencies and clean itself after
# the build
RUN apk update && apk add git && \ 
  go install && \ 
  go mod tidy && \
  go install github.com/a-h/templ/cmd/templ@latest && \
  templ generate && \
  go build -o app . && \
  go clean -cache -modcache && \
  rm -rf "$(which templ)" && \
  apk del git && rm -rf ~/var/cache/apk/*

# Create the image
FROM alpine:3.19.1
WORKDIR /app
# Curl is used for test step in CI/CD Pipeline. But you're free
# to delete it if you don't plan to use it.
RUN apk add curl
COPY --from=builder /go/src/app/app .
COPY --from=builder /go/src/app/locales ./locales
COPY --from=builder /go/src/app/static ./static
EXPOSE 8080
CMD ["./app"]

