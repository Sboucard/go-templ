package middleware

import (
	"embed"
	"fmt"
	"net/http"
	"strings"
)

// Set resources to cache and set content-type
func CacheControlMiddleware(next http.Handler, fs embed.FS) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, ".css") {
			w.Header().Add("Content-Type", "text/css")
		} else if strings.HasPrefix(r.URL.Path, "/static/icons") || strings.HasPrefix(r.URL.Path, "/static/images") {
			switch fileExt := strings.TrimSuffix(strings.TrimPrefix(r.URL.Path, "/static/"), "."); fileExt {
			case "png":
				if !strings.Contains(r.Header.Get("Accept"), "image/png") {
					http.NotFound(w, r)
					return
				}
				w.Header().Set("Content-Type", "image/png")
				w.Header().Set("Cache-Control", "private, max-age=86400")
			case "webp":
				if !strings.Contains(r.Header.Get("Accept"), "image/webp") {
					http.NotFound(w, r)
					return
				}
				w.Header().Set("Content-Type", "image/webp")
				w.Header().Set("Cache-Control", "private, max-age=86400")
			case "jpg", "jpeg":
				if !strings.Contains(r.Header.Get("Accept"), fmt.Sprintf("image/%s", fileExt)) {
					http.NotFound(w, r)
					return
				}
				w.Header().Set("Content-Type", fmt.Sprintf("image/%s", fileExt))
				w.Header().Set("Cache-Control", "private, max-age=86400")
			case "svg":
				if !strings.Contains(r.Header.Get("Accept"), "image/svg") {
					http.NotFound(w, r)
					return
				}
				w.Header().Set("Content-Type", "image/svg")
				w.Header().Set("Cache-Control", "private, max-age=86400")
			}
		}
		next.ServeHTTP(w, r)
	})
}
