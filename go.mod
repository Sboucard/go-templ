module cv/pwa

go 1.22.0

require (
	github.com/a-h/templ v0.2.543
	gopkg.in/yaml.v2 v2.4.0
)
