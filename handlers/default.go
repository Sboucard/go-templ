package handlers

import (
	"bytes"
	"context"
	"cv/pwa/components/page"
	"cv/pwa/helper/helperI18n"
	"cv/pwa/helper/helperServices"
	"cv/pwa/model"

	"cv/pwa/services/careerService"
	"cv/pwa/services/pagesService"
	"fmt"
	"net/http"

	"github.com/a-h/templ"
)

// Register subcat handler route
func RenderRouteHandler() {
	// Render page
	http.HandleFunc("/render", func(w http.ResponseWriter, r *http.Request) {
		viewRoute := r.URL.Query().Get("view")
		helperI18n.ChangeLangNext(w, r)
		helperServices.ComponentStartupService()
		var component templ.Component = pagesService.GetView(viewRoute)

		// Create a buffer that will contain our component in a string
		var buf bytes.Buffer

		errComponent := component.Render(context.Background(), &buf)
		if errComponent != nil {
			http.Error(w, "Error rendering component", http.StatusInternalServerError)
			return
		}

		fmt.Fprint(w, buf.String())
	})

	// Render career card subcat view
	http.HandleFunc("/render-career", func(w http.ResponseWriter, r *http.Request) {
		viewRoute := r.URL.Query().Get("view")
		helperI18n.ChangeLangNext(w, r)
		helperServices.ComponentStartupService()
		component := careerService.GetCareerCardView(viewRoute)

		var buf bytes.Buffer

		err := component.Render(context.Background(), &buf)
		if err != nil {
			http.Error(w, "Error rendering component", http.StatusInternalServerError)
			return
		}

		fmt.Fprint(w, buf.String())
	})
}

// Register buttons handler route
func InitialButtonsHandler() {
	http.HandleFunc("/render-button", func(w http.ResponseWriter, r *http.Request) {
		viewRoute := r.URL.Query().Get("view")
		helperI18n.ChangeLangNext(w, r)
		helperServices.ComponentStartupService()
		var pageModel model.Page = pagesService.GetPage(viewRoute)

		// Create a buffer that will contain our page in a string
		var buf bytes.Buffer

		if pageModel.Title != "" && len(pageModel.Views) >= 0 && pageModel.BaseView != nil {
			errPage := page.PageContainer(pageModel).Render(context.Background(), &buf)
			if errPage != nil {
				http.Error(w, "Error in rendering", http.StatusInternalServerError)
				return
			}
		}

		fmt.Fprint(w, buf.String())
	})
}

// Handle first render and give default component for app
func InitialRenderHandler() {
	http.HandleFunc("/render-init", func(w http.ResponseWriter, r *http.Request) {
		var viewRoute string = "home-subcat-1"
		helperI18n.ChangeLangNext(w, r)
		helperServices.ComponentStartupService()
		var pageModel model.Page = pagesService.GetPage(viewRoute)

		// Create a buffer that will contain our page in a string
		var buf bytes.Buffer

		if pageModel.Title != "" && len(pageModel.Views) >= 0 && pageModel.BaseView != nil {
			errPage := page.PageContainer(pageModel).Render(context.Background(), &buf)
			if errPage != nil {
				http.Error(w, "Error in rendering", http.StatusInternalServerError)
				return
			}
		}

		fmt.Fprint(w, buf.String())
	})

	// Handle app link sticker localization
	http.HandleFunc("/render-init-app-link", func(w http.ResponseWriter, r *http.Request) {
		helperI18n.ChangeLangNext(w, r)
		helperServices.ComponentStartupService()
		w.Header().Set("Content-Type", "text/plain")
		w.Write([]byte(helperI18n.UseLocaleString("app_postit")))
		fmt.Fprint(w)
	})
}
