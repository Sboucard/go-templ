package projectService

import (
	"cv/pwa/helper/helperI18n"
	"cv/pwa/model"
)

var projectCompanyCards []model.ProjectCard
var projectPersonalCards []model.ProjectCard

func InitProjectCards() {
	projectCompanyCards = []model.ProjectCard{
		{
			Title:    helperI18n.UseLocaleString("projects_lbm"),
			Duration: helperI18n.UseLocaleString("projects_lbm_duration"),
			Company:  helperI18n.UseLocaleString("projects_lbm_company"),
			AppURL:   "https://little-big-map.davidson.fr/login",
			Type:     "Company",
			Dates:    helperI18n.UseLocaleString("projects_lbm_dates"),
			Role:     helperI18n.UseLocaleString("projects_lbm_role"),
		},
		{
			Title:    helperI18n.UseLocaleString("projects_impakt"),
			Duration: helperI18n.UseLocaleString("projects_impakt_duration"),
			Company:  helperI18n.UseLocaleString("projects_impakt_company"),
			AppURL:   "https://www.impakt.solutions/",
			Type:     "Company",
			Dates:    helperI18n.UseLocaleString("projects_impakt_dates"),
			Role:     helperI18n.UseLocaleString("projects_impakt_role"),
		},
		{
			Title:    helperI18n.UseLocaleString("projects_m4d"),
			Duration: helperI18n.UseLocaleString("projects_m4d_duration"),
			Company:  helperI18n.UseLocaleString("projects_m4d_company"),
			AppURL:   "https://eval.media4display.com/Media4Display/#/auth/login?returnUrl=%2Fhome",
			Type:     "Company",
			Dates:    helperI18n.UseLocaleString("projects_m4d_dates"),
			Role:     helperI18n.UseLocaleString("projects_m4d_role"),
		},
		{
			Title:    helperI18n.UseLocaleString("projects_info"),
			Duration: helperI18n.UseLocaleString("projects_info_duration"),
			Company:  helperI18n.UseLocaleString("projects_info_company"),
			AppURL:   "https://my-angers.info/",
			Type:     "Company",
			Dates:    helperI18n.UseLocaleString("projects_info_dates"),
			Role:     helperI18n.UseLocaleString("projects_info_role"),
		},
	}

	projectPersonalCards = []model.ProjectCard{
		{
			Title:    helperI18n.UseLocaleString("projects_gotempl"),
			Duration: helperI18n.UseLocaleString("projects_gotempl_duration"),
			AppURL:   "https://gitlab.com/Sboucard/go-templ",
			Type:     "Personal",
			Dates:    helperI18n.UseLocaleString("projects_gotempl_dates"),
			Role:     helperI18n.UseLocaleString("projects_gotempl_role"),
		},
		{
			Title:    helperI18n.UseLocaleString("projects_todo"),
			Duration: helperI18n.UseLocaleString("projects_todo_duration"),
			AppURL:   "https://gitlab.com/Sboucard/todo-pwa",
			Type:     "Personal",
			Dates:    helperI18n.UseLocaleString("projects_todo_dates"),
			Role:     helperI18n.UseLocaleString("projects_todo_role"),
		},
	}
}

// Update the current us
func GetProjectCompanyCards() *[]model.ProjectCard {
	return &projectCompanyCards
}

func GetProjectPersonalCards() *[]model.ProjectCard {
	return &projectPersonalCards
}
