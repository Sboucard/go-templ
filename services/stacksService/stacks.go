package stacksService

import (
	"cv/pwa/helper/helperI18n"
	"cv/pwa/model"
)

var technicalStackCards []model.StackCard
var projectStackCards []model.StackCard
var toolStackCards []model.StackCard
var languagesStackCards []model.StackCard

// Initialize the values of stacks cards in a slice
func InitStackCards() {
	technicalStackCards = []model.StackCard{
		{
			Title:       "Go",
			Mastery:     helperI18n.UseLocaleString("stacks_go_mastery"),
			Description: helperI18n.UseLocaleString("stacks_go_description"),
			ImageURL:    "/static/images/logo_golang.png",
			Type:        "tech",
		},
		{
			Title:       "Javascript",
			Mastery:     helperI18n.UseLocaleString("stacks_js_mastery"),
			Description: helperI18n.UseLocaleString("stacks_js_description"),
			ImageURL:    "/static/images/logo_js.png",
			Type:        "tech",
		},
		{
			Title:       "C#",
			Mastery:     helperI18n.UseLocaleString("stacks_csharp_mastery"),
			Description: helperI18n.UseLocaleString("stacks_csharp_description"),
			ImageURL:    "/static/images/logo_csharp.png",
			Type:        "tech",
		},
		{
			Title:       "ASP.NET Core",
			Mastery:     helperI18n.UseLocaleString("stacks_asp_mastery"),
			Description: helperI18n.UseLocaleString("stacks_asp_description"),
			ImageURL:    "/static/images/logo_asp_core.png",
			Type:        "tech",
		},
		{
			Title:       "Node",
			Mastery:     helperI18n.UseLocaleString("stacks_node_mastery"),
			Description: helperI18n.UseLocaleString("stacks_node_description"),
			ImageURL:    "/static/images/logo_node.png",
			Type:        "tech",
		},
		{
			Title:       "NestJS",
			Mastery:     helperI18n.UseLocaleString("stacks_nestjs_mastery"),
			Description: helperI18n.UseLocaleString("stacks_nestjs_description"),
			ImageURL:    "/static/images/logo_nestjs.png",
			Type:        "tech",
		},
		{
			Title:       "Angular",
			Mastery:     helperI18n.UseLocaleString("stacks_angular_mastery"),
			Description: helperI18n.UseLocaleString("stacks_angular_description"),
			ImageURL:    "/static/images/logo_angular.png",
			Type:        "tech",
		},
		{
			Title:       "ReactJS",
			Mastery:     helperI18n.UseLocaleString("stacks_reactjs_mastery"),
			Description: helperI18n.UseLocaleString("stacks_reactjs_description"),
			ImageURL:    "/static/images/logo_reactjs.webp",
			Type:        "tech",
		},
		{
			Title:       "VueJS",
			Mastery:     helperI18n.UseLocaleString("stacks_vuejs_mastery"),
			Description: helperI18n.UseLocaleString("stacks_vuejs_description"),
			ImageURL:    "/static/images/logo_vuejs.png",
			Type:        "tech",
		},
		{
			Title:       "Tailwind",
			Mastery:     helperI18n.UseLocaleString("stacks_tailwind_mastery"),
			Description: helperI18n.UseLocaleString("stacks_tailwind_description"),
			ImageURL:    "/static/images/logo_tailwind.svg",
			Type:        "tech",
		},
		{
			Title:       "SQL Server",
			Mastery:     helperI18n.UseLocaleString("stacks_sql_server_mastery"),
			Description: helperI18n.UseLocaleString("stacks_sql_server_description"),
			ImageURL:    "/static/images/logo_server.png",
			Type:        "tech",
		},
		{
			Title:       "MongoDB",
			Mastery:     helperI18n.UseLocaleString("stacks_mongo_mastery"),
			Description: helperI18n.UseLocaleString("stacks_mongo_description"),
			ImageURL:    "/static/images/logo_mongodb.png",
			Type:        "tech",
		},
		{
			Title:       "Git",
			Mastery:     helperI18n.UseLocaleString("stacks_git_mastery"),
			Description: helperI18n.UseLocaleString("stacks_git_description"),
			ImageURL:    "/static/images/logo_git.png",
			Type:        "tech",
		},
		{
			Title:       "Docker",
			Mastery:     helperI18n.UseLocaleString("stacks_docker_mastery"),
			Description: helperI18n.UseLocaleString("stacks_docker_description"),
			ImageURL:    "/static/images/logo_docker.webp",
			Type:        "tech",
		},
	}

	projectStackCards = []model.StackCard{
		{
			Title:       "Agile SCRUM",
			Mastery:     helperI18n.UseLocaleString("stacks_scrum_mastery"),
			Description: helperI18n.UseLocaleString("stacks_scrum_description"),
			ImageURL:    "/static/images/logo_scrum.png",
			Type:        "project",
		},
		{
			Title:       "Jenkins",
			Mastery:     helperI18n.UseLocaleString("stacks_jenkins_mastery"),
			Description: helperI18n.UseLocaleString("stacks_jenkins_description"),
			ImageURL:    "/static/images/logo_jenkins.png",
			Type:        "project",
		},
		{
			Title:       "Azure DevOps",
			Mastery:     helperI18n.UseLocaleString("stacks_azure_devops_mastery"),
			Description: helperI18n.UseLocaleString("stacks_azure_devops_description"),
			ImageURL:    "/static/images/logo_azure_devops.png",
			Type:        "project",
		},
		{
			Title:       "Gitlab",
			Mastery:     helperI18n.UseLocaleString("stacks_gitlab_mastery"),
			Description: helperI18n.UseLocaleString("stacks_gitlab_description"),
			ImageURL:    "/static/images/logo_gitlab.png",
			Type:        "project",
		},
		{
			Title:       "Github",
			Mastery:     helperI18n.UseLocaleString("stacks_github_mastery"),
			Description: helperI18n.UseLocaleString("stacks_github_description"),
			ImageURL:    "/static/images/logo_github.png",
			Type:        "project",
		},
	}

	toolStackCards = []model.StackCard{
		{
			Title:       "Neovim/Vim",
			Mastery:     helperI18n.UseLocaleString("stacks_neovim_mastery"),
			Description: helperI18n.UseLocaleString("stacks_neovim_description"),
			ImageURL:    "/static/images/logo_neovim.png",
			Type:        "tool",
		},
		{
			Title:       "Visual Studio",
			Mastery:     helperI18n.UseLocaleString("stacks_vs_mastery"),
			Description: helperI18n.UseLocaleString("stacks_vs_description"),
			ImageURL:    "/static/images/logo_vs.png",
			Type:        "tool",
		},
		{
			Title:       "WSL2",
			Mastery:     helperI18n.UseLocaleString("stacks_wsl2_mastery"),
			Description: helperI18n.UseLocaleString("stacks_wsl2_description"),
			ImageURL:    "/static/images/logo_windows.webp",
			Type:        "tool",
		},
		{
			Title:       "KVM",
			Mastery:     helperI18n.UseLocaleString("stacks_kvm_mastery"),
			Description: helperI18n.UseLocaleString("stacks_kvm_description"),
			ImageURL:    "/static/images/logo_kvm.png",
			Type:        "tool",
		},
	}

	languagesStackCards = []model.StackCard{
		{
			Title:       helperI18n.UseLocaleString("stacks_french_title"),
			Mastery:     helperI18n.UseLocaleString("stacks_french_mastery"),
			Description: helperI18n.UseLocaleString("stacks_french_description"),
			ImageURL:    "/static/images/flag_france.png",
			Type:        "language",
		},
		{
			Title:       helperI18n.UseLocaleString("stacks_english_title"),
			Mastery:     helperI18n.UseLocaleString("stacks_english_mastery"),
			Description: helperI18n.UseLocaleString("stacks_english_description"),
			ImageURL:    "/static/images/flag_uk.webp",
			Type:        "language",
		},
		{
			Title:       helperI18n.UseLocaleString("stacks_japanese_title"),
			Mastery:     helperI18n.UseLocaleString("stacks_japanese_mastery"),
			Description: helperI18n.UseLocaleString("stacks_japanese_description"),
			ImageURL:    "/static/images/flag_japan.png",
			Type:        "language",
		},
	}
}

// Return Technical cards slice
func GetStacksTechnical() *[]model.StackCard {
	return &technicalStackCards
}

// Return Project Management cards slice
func GetStacksManagement() *[]model.StackCard {
	return &projectStackCards
}

// Return Tool cards slice
func GetStacksTool() *[]model.StackCard {
	return &toolStackCards
}

// Return Languages cards slice
func GetStackLanguages() *[]model.StackCard {
	return &languagesStackCards
}
