package buttonsService

import (
	"cv/pwa/helper/helperI18n"
	"cv/pwa/model"
)

var buttons []model.Button

// Create buttons using helper localization
func InitButtons() {
	buttons = []model.Button{
		{Title: helperI18n.UseLocaleString("home_title"), Icon: model.SvgIconHomePath, Route: "home-subcat-1"},
		{Title: helperI18n.UseLocaleString("career_title"), Icon: model.SvgIconCareerPath, Route: "career-subcat-1"},
		{Title: helperI18n.UseLocaleString("about_title"), Icon: model.SvgIconAboutPath, Route: "about-subcat-1"},
		{Title: helperI18n.UseLocaleString("stacks_title"), Icon: model.SvgIconStacksPath, Route: "stacks-subcat-1"},
		{Title: helperI18n.UseLocaleString("projects_title"), Icon: model.SvgIconProjectsPath, Route: "projects-subcat-1"},
	}
}

// Get the buttons slice
func GetButtons() *[]model.Button {
	return &buttons
}
