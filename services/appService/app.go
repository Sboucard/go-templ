package appService

import (
	"cv/pwa/helper/helperI18n"
	"cv/pwa/model"
)

// Sticker link component for github
var appLink model.AppLink

// Initalize the app link sticker data
func InitAppLink() {
	appLink = model.AppLink{
		Title: helperI18n.UseLocaleString("app_postit"),
		URL:   "https://gitlab.com/Sboucard/go-templ",
	}
}

// Return the App Link title value
func GetAppLinkTitle() string {
	return appLink.Title
}

// Return the App Link URL value
func GetAppLinkURL() string {
	return appLink.URL
}
