package cardViewsService

import (
	"cv/pwa/helper/helperI18n"
	"cv/pwa/model"
)

var companyDavidson = model.CompanyInfo{}
var companyTelelogos = model.CompanyInfo{}
var companyBeconfig = model.CompanyInfo{}
var schoolBaccalaureate = model.SchoolInfo{}
var schoolATC = model.SchoolInfo{}
var schoolBachelor = model.SchoolInfo{}
var schoolMaster = model.SchoolInfo{}

// Initialize the values of career card views slices
func InitCardViews() {
	companyDavidson = model.CompanyInfo{
		Duration: helperI18n.UseLocaleString("career_views_davidson_duration"),
		Title:    helperI18n.UseLocaleString("career_views_davidson_title"),
		Projects: []model.Project{
			{
				Title: "Impakt",
			},
			{
				Title: "LBM",
			},
		},
		Stacks: []string{
			"NuxtJS",
			"ReactJS",
			"NestJS",
			"Strapi",
			"Docker",
			"Node",
			"Git",
			"Github / Gitlab",
			"SCRUM",
		},
	}
	companyTelelogos = model.CompanyInfo{
		Duration: helperI18n.UseLocaleString("career_views_telelogos_duration"),
		Title:    helperI18n.UseLocaleString("career_views_telelogos_title"),
		Projects: []model.Project{
			{
				Title: "Media4Display Mobile",
			},
			{
				Title: "Media4Display",
			},
			{
				Title: "MediaContact",
			},
		},
		Stacks: []string{
			"Angular 1",
			"Angular 7",
			"Node",
			".NET Core 3.1",
			"Git",
			"BitBucket",
			"Redmine",
			"SCRUM",
		},
	}
	companyBeconfig = model.CompanyInfo{
		Duration: helperI18n.UseLocaleString("career_views_beconfig_duration"),
		Title:    helperI18n.UseLocaleString("career_views_beconfig_title"),
		Projects: []model.Project{
			{
				Title: helperI18n.UseLocaleString("career_views_beconfig_project"),
			},
		},
		Stacks: []string{
			"Angular 13",
			".NET 6",
			"Git",
			"Azure DevOps",
			"SCRUM",
		},
	}
	schoolBaccalaureate = model.SchoolInfo{
		Duration: helperI18n.UseLocaleString("career_views_chevrollier_baccalaureate_duration"),
		Fields: []string{
			helperI18n.UseLocaleString("career_views_chevrollier_baccalaureate_elec"),
			helperI18n.UseLocaleString("career_views_chevrollier_baccalaureate_programming"),
			helperI18n.UseLocaleString("career_views_chevrollier_baccalaureate_energy"),
			helperI18n.UseLocaleString("career_views_chevrollier_baccalaureate_mecha"),
		},
	}
	schoolATC = model.SchoolInfo{
		Duration: helperI18n.UseLocaleString("career_views_chevrollier_atc_duration"),
		Fields: []string{
			helperI18n.UseLocaleString("career_views_chevrollier_atc_oop"),
			helperI18n.UseLocaleString("career_views_chevrollier_atc_project_management"),
		},
	}
	schoolBachelor = model.SchoolInfo{
		Duration: helperI18n.UseLocaleString("career_views_iia_bachelor_duration"),
		Fields: []string{
			helperI18n.UseLocaleString("career_views_iia_bachelor_programming"),
			helperI18n.UseLocaleString("career_views_iia_bachelor_maths"),
			helperI18n.UseLocaleString("career_views_iia_bachelor_opensource"),
		},
	}
	schoolMaster = model.SchoolInfo{
		Duration: helperI18n.UseLocaleString("career_views_iia_master_duration"),
		Fields: []string{
			helperI18n.UseLocaleString("career_views_iia_master_programming"),
			helperI18n.UseLocaleString("career_views_iia_master_marketing"),
			helperI18n.UseLocaleString("career_views_iia_master_project_management"),
			helperI18n.UseLocaleString("career_views_iia_master_entrepreneurship"),
		},
	}
}

// Return info on Telelogos
func GetCompanyTelelogos() *model.CompanyInfo {
	return &companyTelelogos
}

// Return info on Beconfig
func GetCompanyBeconfig() *model.CompanyInfo {
	return &companyBeconfig
}

// Return info on Davidson Canada
func GetCompanyDavidson() *model.CompanyInfo {
	return &companyDavidson
}

// Return info on baccalaureate
func GetSchoolBaccalaureate() *model.SchoolInfo {
	return &schoolBaccalaureate
}

// Return info on ATC
func GetSchoolATC() *model.SchoolInfo {
	return &schoolATC
}

// Return info on bachelor
func GetSchoolBachelor() *model.SchoolInfo {
	return &schoolBachelor
}

// Return info on master
func GetSchoolMaster() *model.SchoolInfo {
	return &schoolMaster
}
