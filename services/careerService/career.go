package careerService

import (
	"cv/pwa/helper/helperI18n"
	"cv/pwa/model"
	"cv/pwa/views/careerView/careerCardViews"
	"fmt"

	"github.com/a-h/templ"
)

var professionalCareerCards []model.CareerCard
var schoolCareerCards []model.CareerCard
var careerMap = make(map[string]templ.Component)

// Initialize the values of professional & school slices
func InitCareerCards() {
	professionalCareerCards = []model.CareerCard{
		{
			Title:        helperI18n.UseLocaleString("career_davidson"),
			Period:       helperI18n.UseLocaleString("career_davidson_period"),
			Location:     helperI18n.UseLocaleString("career_davidson_location"),
			Group:        "davidson",
			BaseCardView: careerCardViews.DavContext(),
			Content: model.CareerCardContent{
				ID: "carousel-davidson",
				Subcat: []model.CareerSubcat{
					{
						Index:          1,
						Title:          helperI18n.UseLocaleString("career_davidson_subcat_context"),
						Route:          "davidson-carousel-1",
						CareerCardView: careerCardViews.DavContext(),
					},
					{
						Index:          2,
						Title:          helperI18n.UseLocaleString("career_davidson_subcat_mission"),
						Route:          "davidson-carousel-2",
						CareerCardView: careerCardViews.DavRole(),
					},
				},
			},
		},
		{
			Title:        helperI18n.UseLocaleString("career_beconfig"),
			Period:       helperI18n.UseLocaleString("career_beconfig_period"),
			Location:     helperI18n.UseLocaleString("career_beconfig_location"),
			Group:        "beconfig",
			BaseCardView: careerCardViews.BeconfigContext(),
			Content: model.CareerCardContent{
				ID: "carousel-beconfig",
				Subcat: []model.CareerSubcat{
					{
						Index:          1,
						Title:          helperI18n.UseLocaleString("career_beconfig_subcat_context"),
						Route:          "beconfig-carousel-1",
						CareerCardView: careerCardViews.BeconfigContext(),
					},
					{
						Index:          2,
						Title:          helperI18n.UseLocaleString("career_beconfig_subcat_mission"),
						Route:          "beconfig-carousel-2",
						CareerCardView: careerCardViews.BeconfigRole(),
					},
				},
			},
		},
		{
			Title:        helperI18n.UseLocaleString("career_telelogos"),
			Period:       helperI18n.UseLocaleString("career_telelogos_period"),
			Location:     helperI18n.UseLocaleString("career_telelogos_location"),
			BaseCardView: careerCardViews.TelelogosContext(),
			Group:        "telelogos",
			Content: model.CareerCardContent{
				ID: "carousel-telelogos",
				Subcat: []model.CareerSubcat{
					{
						Index:          1,
						Title:          helperI18n.UseLocaleString("career_telelogos_subcat_context"),
						Route:          "telelogos-carousel-1",
						CareerCardView: careerCardViews.TelelogosContext(),
					},
					{
						Index:          2,
						Title:          helperI18n.UseLocaleString("career_telelogos_subcat_mission"),
						Route:          "telelogos-carousel-2",
						CareerCardView: careerCardViews.TelelogosRole(),
					},
				},
			},
		},
	}
	schoolCareerCards = []model.CareerCard{
		{
			Title:        helperI18n.UseLocaleString("career_iia"),
			Period:       helperI18n.UseLocaleString("career_iia_period"),
			Location:     helperI18n.UseLocaleString("career_iia_location"),
			Group:        "iia",
			BaseCardView: careerCardViews.IIABachelor(),
			Content: model.CareerCardContent{
				ID: "carousel-iia",
				Subcat: []model.CareerSubcat{
					{
						Index:          1,
						Title:          helperI18n.UseLocaleString("career_iia_bachelor"),
						Route:          "iia-carousel-1",
						CareerCardView: careerCardViews.IIABachelor(),
					},
					{
						Index:          2,
						Title:          helperI18n.UseLocaleString("career_iia_master"),
						Route:          "iia-carousel-2",
						CareerCardView: careerCardViews.IIAMaster(),
					},
				},
			},
		},
		{
			Title:        helperI18n.UseLocaleString("career_chevrollier"),
			Period:       helperI18n.UseLocaleString("career_chevrollier_period"),
			Location:     helperI18n.UseLocaleString("career_chevrollier_location"),
			BaseCardView: careerCardViews.ChevrollierBaccalaureate(),
			Group:        "chevrollier",
			Content: model.CareerCardContent{
				ID: "carousel-chevrollier",
				Subcat: []model.CareerSubcat{
					{
						Index:          1,
						Title:          helperI18n.UseLocaleString("career_chevrollier_baccalaureate"),
						Route:          "chevrollier-carousel-1",
						CareerCardView: careerCardViews.ChevrollierBaccalaureate(),
					},
					{
						Index:          2,
						Title:          helperI18n.UseLocaleString("career_chevrollier_atc"),
						Route:          "chevrollier-carousel-2",
						CareerCardView: careerCardViews.ChevrollierATC(),
					},
				},
			},
		},
	}
}

// Create a map between route and path for professional & school slices
func InitMap() {
	for _, professionalCareerCard := range professionalCareerCards {
		for _, subcatView := range professionalCareerCard.Content.Subcat {
			careerMap[subcatView.Route] = subcatView.CareerCardView
		}
	}
	for _, schoolCareerCard := range schoolCareerCards {
		for _, subcatView := range schoolCareerCard.Content.Subcat {
			careerMap[subcatView.Route] = subcatView.CareerCardView
		}
	}
}

// Return professional cards slice
func GetProfessionalCards() *[]model.CareerCard {
	return &professionalCareerCards
}

// Return school cards slice
func GetSchoolCards() *[]model.CareerCard {
	return &schoolCareerCards
}

// Return card view
func GetCareerCardView(careerCardViewRoute string) templ.Component {
	careerCardView, ok := careerMap[careerCardViewRoute]
	if !ok {
		fmt.Errorf("View not found")
	}
	return careerCardView
}
