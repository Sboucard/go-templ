package pagesService

import (
	"cv/pwa/helper/helperI18n"
	"cv/pwa/model"
	"cv/pwa/views/aboutView"
	"cv/pwa/views/careerView"
	"cv/pwa/views/homeView"
	"cv/pwa/views/projectsView"
	"cv/pwa/views/stacksView"
	"fmt"

	"github.com/a-h/templ"
)

var pages []model.Page
var viewMap = make(map[string]templ.Component)
var pageMap = make(map[string]model.Page)

// Initialize the values of pages inside the app in a slice
func InitPages() {
	pages = []model.Page{
		{
			Title: helperI18n.UseLocaleString("home_title"), Views: []model.View{
				{
					Label: helperI18n.UseLocaleString("home_intro"), Component: homeView.Intro(), Route: "home-subcat-1",
				},
				{
					Label: helperI18n.UseLocaleString("home_resume"), Component: homeView.Resume(), Route: "home-subcat-2",
				},
			},
			BaseView: homeView.Intro(),
		},
		{
			Title: helperI18n.UseLocaleString("career_title"), Views: []model.View{
				{
					Label: helperI18n.UseLocaleString("career_intro"), Component: careerView.Intro(), Route: "career-subcat-1",
				},
				{
					Label: helperI18n.UseLocaleString("career_school"), Component: careerView.School(), Route: "career-subcat-2",
				},
			},
			BaseView: careerView.Intro(),
		},
		{
			Title: helperI18n.UseLocaleString("about_title"), Views: []model.View{
				{
					Label: helperI18n.UseLocaleString("about_intro"), Component: aboutView.Intro(), Route: "about-subcat-1",
				},
				{
					Label: helperI18n.UseLocaleString("about_todos"), Component: aboutView.Projects(), Route: "about-subcat-2",
				},
			},
			BaseView: aboutView.Intro(),
		},
		{
			Title: helperI18n.UseLocaleString("stacks_title"), Views: []model.View{
				{
					Label: helperI18n.UseLocaleString("stacks_intro"), Component: stacksView.Intro(), Route: "stacks-subcat-1",
				},
				{
					Label: helperI18n.UseLocaleString("stacks_management"), Component: stacksView.Management(), Route: "stacks-subcat-2",
				},
				{
					Label: helperI18n.UseLocaleString("stacks_tools"), Component: stacksView.Tools(), Route: "stacks-subcat-3",
				},
				{
					Label: helperI18n.UseLocaleString("stacks_languages"), Component: stacksView.Languages(), Route: "stacks-subcat-4",
				},
			},
			BaseView: stacksView.Intro(),
		},
		{
			Title: helperI18n.UseLocaleString("projects_title"), Views: []model.View{
				{
					Label: helperI18n.UseLocaleString("projects_intro"), Component: projectsView.Intro(), Route: "projects-subcat-1",
				},
				{
					Label: helperI18n.UseLocaleString("projects_personal"), Component: projectsView.Personal(), Route: "projects-subcat-2",
				},
			},
			BaseView: projectsView.Intro(),
		},
	}
}

// Create a record between pages and route
func InitMap() {
	for _, page := range pages {
		for _, view := range page.Views {
			viewMap[view.Route] = view.Component
			pageMap[view.Route] = page
		}
	}
}

// Return View
func GetView(viewRoute string) templ.Component {
	view, ok := viewMap[viewRoute]
	if !ok {
		fmt.Errorf("View not found")
	}
	return view
}

// Return Page
func GetPage(viewRoute string) model.Page {
	page, ok := pageMap[viewRoute]
	if !ok {
		fmt.Errorf("Page not found")
	}
	return page
}

// Getter Pages
func GetPages() *[]model.Page {
	return &pages
}

// Getter ViewMap
func GetViewMap() *map[string]templ.Component {
	return &viewMap
}

// Getter PageMap
func GetViewPage() *map[string]model.Page {
	return &pageMap
}
