package aboutService

import (
	"cv/pwa/helper/helperI18n"
	"cv/pwa/model"
)

// Slice containing the todos element
var todos []model.Todo

// Initialize the todos slice
func InitTodo() {
	todos = []model.Todo{
		{
			Label:    helperI18n.UseLocaleString("about_todos_master"),
			Checked:  true,
			Disabled: true,
		},
		{
			Label:    helperI18n.UseLocaleString("about_todos_canada"),
			Checked:  true,
			Disabled: true,
		},
		{
			Label:    helperI18n.UseLocaleString("about_todos_montreal_work"),
			Checked:  true,
			Disabled: true,
		},
		{
			Label:    helperI18n.UseLocaleString("about_todos_montreal_marathon"),
			Checked:  true,
			Disabled: true,
		},
		{
			Label:    helperI18n.UseLocaleString("about_todos_japan"),
			Checked:  false,
			Disabled: false,
		},
		{
			Label:    helperI18n.UseLocaleString("about_todos_japan_work"),
			Checked:  false,
			Disabled: false,
		},
	}
}

// Return the todos slice
func GetTodos() *[]model.Todo {
	return &todos
}
