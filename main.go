package main

import (
	"cv/pwa/components/app"
	"cv/pwa/handlers"
	"cv/pwa/helper/helperI18n"
	"cv/pwa/helper/helperServices"
	"cv/pwa/middleware"
	"embed"
	"fmt"
	"net/http"
	"strings"

	"github.com/a-h/templ"
)

//go:embed static
var static embed.FS

//go:embed assets
var assets embed.FS

//go:generate npm run build
func main() {

	helperI18n.RegisterI18NextBundle()

	// Register App & CSS
	h := setLocalizerMiddleware(app.App)

	// Make sure that css file are Content-Type "css"
	http.Handle("/static/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		middleware.CacheControlMiddleware(http.StripPrefix("/static", http.FileServer(http.Dir("./static"))), static).ServeHTTP(w, r)
	}))

	// Make sure that js files are Content-Type "javascript"
	http.Handle("/assets/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, ".js") {
			w.Header().Add("Content-Type", "application/javscript")
		}
		http.FileServer(http.FS(assets)).ServeHTTP(w, r)
	}))

	// Display the templ App
	http.Handle("/", h)

	// Register route handlers
	handlersSetup()

	fmt.Println("Listening on :8080")
	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		fmt.Printf("Server failed to start: %v", err)
	}
}

// Setup and serve Templ Components with current locale
func setLocalizerMiddleware(next func() templ.Component) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Initialize the components data
		helperServices.ComponentStartupService()
		c := next()
		h := templ.Handler(c)
		h.ServeHTTP(w, r)
	}
}

// Setup route handlers
func handlersSetup() {
	handlers.InitialRenderHandler()
	handlers.InitialButtonsHandler()
	handlers.RenderRouteHandler()
}
